# Getting Started

### Reference Documentation
JobController Endpoint: http://localhost:8080/jobs/csv-processing
DB Console: http://localhost:8080/h2-console

### Additional Links
These additional references should also help you:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)

