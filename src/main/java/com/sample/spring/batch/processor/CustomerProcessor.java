package com.sample.spring.batch.processor;

import com.sample.spring.batch.entity.Customer;
import org.springframework.batch.item.ItemProcessor;

public class CustomerProcessor implements ItemProcessor<Customer, Customer> {
    @Override
    public Customer process(Customer customer)
    {
        if(customer.getCountry().equalsIgnoreCase("United States")) {
            return customer;
        }
        else{
            return null;
        }

    }
}
